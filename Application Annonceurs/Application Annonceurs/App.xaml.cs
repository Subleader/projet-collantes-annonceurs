﻿using Notes.Services;
using Notes.Views;
using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notes
{
    public partial class App : Application
    {

        IFirebaseAuthentication auth;
        public static string FolderPath { get; private set; }
        public static string UserToken { get; set; }

        public App()
        {
            InitializeComponent();
            FolderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));

            auth = DependencyService.Get<IFirebaseAuthentication>();
            if (auth.IsSignIn())
            {
                MainPage = new AppShell();
            }
            else
            {
                MainPage = new LancementPage();
       
            }
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}