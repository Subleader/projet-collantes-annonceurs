﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Notes.Models;
using Notes.ViewModels;
using Application_Annonceurs;
using Application_Annonceurs.views;

namespace Notes.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Annonces : ContentPage
    {
        VMAnnonces vmAnnonce;
        public Annonces()
        {
            Console.WriteLine("ICI");
            InitializeComponent();
            vmAnnonce = new VMAnnonces();
            this.BindingContext = vmAnnonce;
        }

        private async void lstAnnonces_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                if (lstAnnonces.SelectedItem != null)
                {
                    Annonces annonce = (Annonces)e.SelectedItem;
                    if (annonce != null)
                    {
                        var display = await DisplayActionSheet(annonce.Title, "Cancel",
                        null, new string[] { "Edit", "Delete" });
                        if (display.Equals("Edit"))
                        {
                            vmAnnonce.setAnnonce(annonce);
                        }
                        else if (display.Equals("Delete"))
                        {
                            vmAnnonce.setAnnonce(annonce);
                            await vmAnnonce.trnAnnonces("DELETE");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            lstAnnonces.SelectedItem = null;
        }

        async void OnAddClicked(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync(nameof(NoteEntryPage));
        }

        async void OnLocalisationClicked(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync(nameof(LocationPage));

        }


        async void FilterClicked(object sender, EventArgs e)
        {
            // Navigate to the NoteEntryPage, without passing any data.
            await Shell.Current.GoToAsync(nameof(NoteEntryPage));
        }
    }
}