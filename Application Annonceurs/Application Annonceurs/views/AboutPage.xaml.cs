﻿using Notes.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace Notes.Views
{
    public partial class AboutPage : ContentPage
    {
        IFirebaseAuthentication auth;
        public AboutPage()
        {
            InitializeComponent();
            auth = DependencyService.Get<IFirebaseAuthentication>();
        }

        async void OnButtonClicked(object sender, EventArgs e)
        {
            // Launch the specified URL in the system browser.
            var signedOut = auth.SignOut();
            if (signedOut)
            {
                await Navigation.PushModalAsync(new LancementPage());
            }
        }
    }
}