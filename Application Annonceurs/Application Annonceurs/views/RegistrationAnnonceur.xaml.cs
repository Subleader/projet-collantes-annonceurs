﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Notes.Models;
using Notes.ViewModels;


namespace Notes.Views
{
   public partial class RegistrationAnnonceur : ContentPage
    {
         object image1;

        public RegistrationAnnonceur()
        {
            Inscription Inscription;

            InitializeComponent();
            Inscription = new Inscription();
            this.BindingContext = Inscription;
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();
            //connexion à la base de donnée

        }
         public async void LoadTakePictiure(object sender, EventArgs e)
        {
            
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsCameraAvailable || CrossMedia.Current.IsTakePhotoSupported)
            {
                //Charger une photo
                await DisplayAlert("pas de camera", "Aucune camera disponible","OK");
                return;
            }
            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = true,
                Name = "Profile.jpg"
            });
            if (file == null)
            {
                return;
            }
            image1 = ImageSource.FromStream(() => file.GetStream());
        }

        public async void OnTakePictiure(object sender, EventArgs e)
        {
            //prendre une photo
            if (!CrossMedia.Current.IsPickPhotoSupported || CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("pas de camera", "de charger une photo", "OK");
                return;
            }
            var file = await CrossMedia.Current.PickPhotoAsync();
            
            if (file == null)
            {
                return;
            }
            image1 = ImageSource.FromStream(() => file.GetStream());
        }
    }
}