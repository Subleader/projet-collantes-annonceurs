﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Notes.Models;
using Notes.ViewModels;

namespace Notes.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShowAnnonce : ContentPage
    {
        VMAnnonces vmAnnonce;
        public ShowAnnonce()
        {
            InitializeComponent();
            vmAnnonce = new VMAnnonces();
            this.BindingContext = vmAnnonce;
        }
    }
}