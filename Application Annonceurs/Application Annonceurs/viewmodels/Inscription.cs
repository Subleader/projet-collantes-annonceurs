﻿using Firebase.Database;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Notes.Models;

namespace Notes.ViewModels
{
    public class Inscription : INotifyPropertyChanged
    {
        FirebaseClient fClient;

        private RegisterUser _registerUser { get; set; }

        public RegisterUser registerUser
        {
            get { return _registerUser; }
            set
            {
                _registerUser = value;
                OnPropertyChanged();
            }
        }

        private bool _showButton { get; set; }
        public bool showButton
        {
            get { return _showButton; }
            set
            {
                _showButton = value;
                OnPropertyChanged();
            }
        }
        private bool _isBusy { get; set; }
        public bool isBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
                showButton = !value;
            }
        }

        internal void setInscription(Views.RegistrationAnnonceur registrationAnnonceur)
        {
            throw new NotImplementedException();
        }

        private ICommand _btnSaveAnnonce { get; set; }
        public ICommand btnSaveAnnonce
        {
            get { return _btnSaveAnnonce; }
            set
            {
                _btnSaveAnnonce = value;
                OnPropertyChanged();
            }
        }

        private string _lblMessage { get; set; }
        public string lblMessage
        {
            get { return _lblMessage; }
            set
            {
                _lblMessage = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<RegisterUser> _lstAnnonces { get; set; }

        public ObservableCollection<RegisterUser> lstAnnonces
        {
            get { return _lstAnnonces; }
            set
            {
                _lstAnnonces = value;
                OnPropertyChanged();
            }
        }

        private string _btnSaveText { get; set; }
        public string btnSaveText
        {
            get { return _btnSaveText; }
            set
            {
                _btnSaveText = value;
                OnPropertyChanged();
            }
        }

        readonly string annonceResource = "Annonces";
        public Inscription()
        {
            try
            {
                lstAnnonces = new ObservableCollection<RegisterUser>();
                btnSaveAnnonce = new Command(async () =>
                {
                    isBusy = true;
                    await trnAnnonces("ADD");
                });
                var callList = new Command(async () => await GetAllAnnonces());
                callList.Execute(null);

            }
            catch (Exception ex)
            {
                lblMessage = "Error occurred " + ex.Message.ToString();
            }
        }

        public bool connectFirebase()
        {
            try
            {
                if (fClient == null)
                    fClient = new FirebaseClient("https://annonceurs-6fdec-default-rtdb.europe-west1.firebasedatabase.app/");
                return true;
            }
            catch (Exception ex)
            {
                lblMessage = "Error occurred in connecting firebase. Error:" + ex.Message.ToString();
                return false;
            }

        }

        public async Task trnAnnonces(string action)
        {
            try
            {
                if (registerUser == null || String.IsNullOrWhiteSpace(registerUser.username) || registerUser.adresse == null)
                {
                  
                    lblMessage = "Veuillez entrer les details de votre annonce";
                    isBusy = false;
                    return;
                }

                if (connectFirebase())
                {
                    RegisterUser users = new RegisterUser();
                    users.username = registerUser.username;
                    users.password = registerUser.password;
                    users.email = registerUser.email;
                    users.adresse = registerUser.adresse;
                    users.phone = registerUser.phone;

                    if (btnSaveText == "SAVE" && action.Equals("ADD"))
                    {
                        users.userId = Guid.NewGuid();

                        await fClient.Child(annonceResource).PostAsync(JsonConvert.SerializeObject(users));
                        lblMessage = "Annonce ajoutée avec succès";
                        Application.Current.MainPage = new AppShell();
                    }
                    else if (btnSaveText == "UPDATE" && action.Equals("ADD"))
                    {
                        users.userId = registerUser.userId;

                        var updateAnnonce = (await fClient.Child(annonceResource).OnceAsync<RegisterUser>()).FirstOrDefault(x => x.Object.userId == users.userId);

                        if (updateAnnonce == null)
                        {
                            lblMessage = "Cannot find selected Annonce";
                            isBusy = false;
                            return;
                        }
                        await fClient
                        .Child(annonceResource + "/" + updateAnnonce.Key).PatchAsync(JsonConvert.SerializeObject(users));
                        await GetAllAnnonces();
                        lblMessage = "Annonce mise à jour avec succès";
                    }
                    else if (action.Equals("DELETE"))
                    {
                        var deleteAnnonce = (await fClient
                        .Child(annonceResource)
                        .OnceAsync<RegisterUser>()).FirstOrDefault(d => d.Object.userId == registerUser.userId);

                        if (deleteAnnonce == null)
                        {
                            lblMessage = "Cannot find selected Annonce";
                            isBusy = false;
                            return;
                        }

                        await fClient
                        .Child(annonceResource + "/" + deleteAnnonce.Key).DeleteAsync();

                        await GetAllAnnonces();
                        lblMessage = "Annonce supprimée avec succès";
                    }

                }
            }
            catch (Exception ex)
            {
                lblMessage = "Error occurred. Cannot save Annonce. Error:" + ex.Message.ToString();

            }
            isBusy = false;
        }

        private Task DisplayAlert(string v1, string v2, string v3)
        {
            throw new NotImplementedException();
        }

        public async Task GetAllAnnonces()
        {
            Clear();
            isBusy = true;
            try
            {
                lstAnnonces = new ObservableCollection<RegisterUser>();
                if (connectFirebase())
                {
                    var lst = (await fClient.Child(annonceResource).OnceAsync<RegisterUser>()).Select(x =>
                    new RegisterUser
                    {
                        userId = x.Object.userId,
                        username = x.Object.username,
                        password = x.Object.password,
                        email = x.Object.email,
                        adresse = x.Object.adresse,
                        phone = x.Object.phone
                    }).ToList();

                    lstAnnonces = new ObservableCollection<RegisterUser>(lst.OrderByDescending(l => l.username));
                }
            }
            catch (Exception ex)
            {
                lblMessage = "Error occurred in getting annonces. Error:" + ex.Message.ToString();
            }
            isBusy = false;
        }

        public void setAnnonce(RegisterUser edt)
        {
            registerUser = new RegisterUser();
            registerUser.username = edt.username;
            registerUser.email = edt.email;
            registerUser.password = edt.password;
            registerUser.adresse = edt.adresse;
            registerUser.phone = edt.phone;
            btnSaveText = "UPDATE";
            registerUser.userId = edt.userId;
        }

        public void Clear()
        {
            registerUser = new RegisterUser();
            registerUser.email = "";
            registerUser.username = "";
            registerUser.password = "";
            registerUser.adresse = "";
            registerUser.phone = "";
            isBusy = false;
            registerUser.userId = Guid.Empty;
            btnSaveText = "SAVE";
            lblMessage = "";
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}