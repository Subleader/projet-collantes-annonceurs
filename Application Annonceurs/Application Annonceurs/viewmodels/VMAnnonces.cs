﻿using Firebase.Database;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Notes.Models;

namespace Notes.ViewModels
{
    public class VMAnnonces : INotifyPropertyChanged
    {
        FirebaseClient fClient;

        private Annonces _annonce { get; set; }

        public Annonces annonce
        {
            get { return _annonce; }
            set
            {
                _annonce = value;
                OnPropertyChanged();
            }
        }

        private bool _showButton { get; set; }
        public bool showButton
        {
            get { return _showButton; }
            set
            {
                _showButton = value;
                OnPropertyChanged();
            }
        }
        private bool _isBusy { get; set; }
        public bool isBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
                showButton = !value;
            }
        }

        internal void setAnnonce(Views.Annonces annonce)
        {
            throw new NotImplementedException();
        }

        private ICommand _btnSaveAnnonce { get; set; }
        public ICommand btnSaveAnnonce
        {
            get { return _btnSaveAnnonce; }
            set
            {
                _btnSaveAnnonce = value;
                OnPropertyChanged();
            }
        }

        private string _lblMessage { get; set; }
        public string lblMessage
        {
            get { return _lblMessage; }
            set
            {
                _lblMessage = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<Annonces> _lstAnnonces { get; set; }

        public ObservableCollection<Annonces> lstAnnonces
        {
            get { return _lstAnnonces; }
            set
            {
                _lstAnnonces = value;
                OnPropertyChanged();
            }
        }

        private string _btnSaveText { get; set; }
        public string btnSaveText
        {
            get { return _btnSaveText; }
            set
            {
                _btnSaveText = value;
                OnPropertyChanged();
            }
        }

        readonly string annonceResource = "Annonces";
        public VMAnnonces()
        {
            try
            {
                lstAnnonces = new ObservableCollection<Annonces>();
                btnSaveAnnonce = new Command(async () =>
                {
                    isBusy = true;
                    await trnAnnonces("ADD");
                });
                var callList = new Command(async () => await GetAllAnnonces());
                callList.Execute(null);

            }
            catch (Exception ex)
            {
                lblMessage = "Error occurred " + ex.Message.ToString();
            }
        }

        public bool connectFirebase()
        {
            try
            {
                if (fClient == null)
                    fClient = new FirebaseClient("https://annonceurs-6fdec-default-rtdb.europe-west1.firebasedatabase.app/");
                return true;
            }
            catch (Exception ex)
            {
                lblMessage = "Error occurred in connecting firebase. Error:" + ex.Message.ToString();
                return false;
            }

        }

        public async Task trnAnnonces(string action)
        {
            try
            {
                if (annonce == null || String.IsNullOrWhiteSpace(annonce.titre) || annonce.date == null)
                {
                  
                    lblMessage = "Veuillez entrer les details de votre annonce";
                    isBusy = false;
                    return;
                }

                if (connectFirebase())
                {
                    Annonces annonces = new Annonces();
                    annonces.titre = annonce.titre;
                    annonces.description = annonce.description;
                    annonces.ville = annonce.ville;
                    annonces.adresse = annonce.adresse;
                    annonces.codePostal = annonce.codePostal;
                    annonces.annonceur = App.UserToken;
                    annonces.date = DateTime.Now;

                    if (btnSaveText == "SAVE" && action.Equals("ADD"))
                    {
                        annonces.annonceId = Guid.NewGuid();

                        await fClient.Child(annonceResource).PostAsync(JsonConvert.SerializeObject(annonces));
                        lblMessage = "Annonce ajoutée avec succès";
                        Application.Current.MainPage = new AppShell();
                    }
                    else if (btnSaveText == "UPDATE" && action.Equals("ADD"))
                    {
                        annonces.annonceId = annonce.annonceId;

                        var updateAnnonce = (await fClient.Child(annonceResource).OnceAsync<Annonces>()).FirstOrDefault(x => x.Object.annonceId == annonces.annonceId);

                        if (updateAnnonce == null)
                        {
                            lblMessage = "Cannot find selected Annonce";
                            isBusy = false;
                            return;
                        }
                        await fClient
                        .Child(annonceResource + "/" + updateAnnonce.Key).PatchAsync(JsonConvert.SerializeObject(annonces));
                        await GetAllAnnonces();
                        lblMessage = "Annonce mise à jour avec succès";
                    }
                    else if (action.Equals("DELETE"))
                    {
                        var deleteAnnonce = (await fClient
                        .Child(annonceResource)
                        .OnceAsync<Annonces>()).FirstOrDefault(d => d.Object.annonceId == annonce.annonceId);

                        if (deleteAnnonce == null)
                        {
                            lblMessage = "Cannot find selected Annonce";
                            isBusy = false;
                            return;
                        }

                        await fClient
                        .Child(annonceResource + "/" + deleteAnnonce.Key).DeleteAsync();

                        await GetAllAnnonces();
                        lblMessage = "Annonce supprimée avec succès";
                    }

                }
            }
            catch (Exception ex)
            {
                lblMessage = "Error occurred. Cannot save Annonce. Error:" + ex.Message.ToString();

            }
            isBusy = false;
        }

        private Task DisplayAlert(string v1, string v2, string v3)
        {
            throw new NotImplementedException();
        }

        public async Task GetAllAnnonces()
        {
            Clear();
            isBusy = true;
            try
            {
                lstAnnonces = new ObservableCollection<Annonces>();
                if (connectFirebase())
                {
                    var lst = (await fClient.Child(annonceResource).OnceAsync<Annonces>()).Select(x =>
                    new Annonces
                    {
                        annonceId = x.Object.annonceId,
                        titre = x.Object.titre,
                        description = x.Object.description,
                        ville = x.Object.ville,
                        adresse = x.Object.adresse,
                        codePostal = x.Object.codePostal,
                        annonceur = x.Object.annonceur,
                        date = x.Object.date,
                    }).ToList();

                    lstAnnonces = new ObservableCollection<Annonces>(lst.OrderByDescending(l => l.date));
                }
            }
            catch (Exception ex)
            {
                lblMessage = "Error occurred in getting annonces. Error:" + ex.Message.ToString();
            }
            isBusy = false;
        }

        public void setAnnonce(Annonces edt)
        {
            annonce = new Annonces();
            annonce.titre = edt.titre;
            annonce.ville = edt.ville;
            annonce.adresse = edt.adresse;
            annonce.codePostal = edt.codePostal;
            annonce.annonceur = edt.annonceur;
            annonce.date = edt.date;
            btnSaveText = "UPDATE";
            annonce.annonceId = edt.annonceId;
        }

        public void Clear()
        {
            annonce = new Annonces();
            annonce.titre = "";
            annonce.description = "";
            annonce.ville = "";
            annonce.adresse = "";
            annonce.codePostal = "";
            annonce.annonceur = App.UserToken;
            annonce.date = DateTime.Now;
            isBusy = false;
            annonce.annonceId = Guid.Empty;
            btnSaveText = "SAVE";
            lblMessage = "";
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}