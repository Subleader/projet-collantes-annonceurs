﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application_Annonceurs.viewmodels
{
    public class Annonceur
    {
        public Guid AnnonceurId{ get; set; }
        public string AnnonceurName { get; set; }
        public string AnnonceurEmail { get; set; }
        public string AnnonceurPassword{ get; set; }
        public string AnnonceurAdress { get; set; }
        public string AnnonceurPhone { get; set; }
        //public String AnnonceurPhoto { get; set; }

    }
}
