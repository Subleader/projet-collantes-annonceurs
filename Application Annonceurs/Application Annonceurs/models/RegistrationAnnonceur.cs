﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Notes.Models
{
    public class RegisterUser : INotifyPropertyChanged
    {
        private Guid _userId { get; set; }

        public Guid userId
        {
            get { return _userId; }
            set
            {
                _userId = value; OnPropertyChanged();
            }
        }
        private string _username { get; set; }

        public string username
        {
            get { return _username; }
            set
            {
                _username = value; OnPropertyChanged();
            }
        }

        private string _password { get; set; }

        public string password
        {
            get { return _password; }
            set
            {
                _password = value; OnPropertyChanged();
            }
        }

        private string _email { get; set; }

        public string email
        {
            get { return _email; }
            set
            {
                _email = value; OnPropertyChanged();
            }
        }

        private string _adresse { get; set; }

        public string adresse
        {
            get { return _adresse; }
            set
            {
                _adresse = value; OnPropertyChanged();
            }
        }

        private string _phone { get; set; }

        public string phone
        {
            get { return _phone; }
            set
            {
                _phone = value; OnPropertyChanged();
            }
        }
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}