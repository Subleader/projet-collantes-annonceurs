﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Notes.Models
{
    public class Annonces : INotifyPropertyChanged
    {
        private Guid _annonceId { get; set; }

        public Guid annonceId
        {
            get { return _annonceId; }
            set
            {
                _annonceId = value; OnPropertyChanged();
            }
        }
        private string _titre { get; set; }

        public string titre
        {
            get { return _titre; }
            set
            {
                _titre = value; OnPropertyChanged();
            }
        }

        private string _description { get; set; }

        public string description
        {
            get { return _description; }
            set
            {
                _description = value; OnPropertyChanged();
            }
        }

        private string _ville { get; set; }

        public string ville
        {
            get { return _ville; }
            set
            {
                _ville = value; OnPropertyChanged();
            }
        }

        private string _adresse { get; set; }

        public string adresse
        {
            get { return _adresse; }
            set
            {
                _adresse = value; OnPropertyChanged();
            }
        }

        private string _codePostal { get; set; }

        public string codePostal
        {
            get { return _codePostal; }
            set
            {
                _codePostal = value; OnPropertyChanged();
            }
        }

        private string _annonceur { get; set; }

        public string annonceur
        {
            get { return _annonceur; }
            set
            {
                _annonceur = value; OnPropertyChanged();
            }
        }

        private DateTime _date { get; set; }

        public DateTime date
        {
            get { return _date; }
            set
            {
                _date = value; OnPropertyChanged();
            }
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}